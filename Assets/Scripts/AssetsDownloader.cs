﻿using System;
using UnityEngine;
using System.Collections; 

class AssetsDownloader : MonoBehaviour {
   public string bundleURL;
   public string assetName;

   IEnumerator Start() {
     // Download the file from the URL. It will not be saved in the Cache
     using (WWW www = new WWW(bundleURL)) {
         yield return www;
         if (www.error != null)
             throw new Exception("WWW download had an error:" + www.error);
		else {
			 AssetBundle bundle = www.assetBundle;
             Sprite wallHorizontal = (Sprite)bundle.LoadAsset("wallHorizontal", typeof(Sprite));
             Sprite wallVertical = (Sprite)bundle.LoadAsset("WallVertical", typeof(Sprite));
             Sprite dottedLine = (Sprite)bundle.LoadAsset("dottedLine", typeof(Sprite));
             Sprite racket = (Sprite)bundle.LoadAsset("Racket", typeof(Sprite));
             Sprite ball = (Sprite)bundle.LoadAsset("Ball", typeof(Sprite));

			 GameController.Instance.leftWallGO.GetComponent<SpriteRenderer>().sprite = wallVertical;
			 GameController.Instance.rightWallGO.GetComponent<SpriteRenderer>().sprite = wallVertical;
			 GameController.Instance.bottomWallGO.GetComponent<SpriteRenderer>().sprite = wallHorizontal;
			 GameController.Instance.topWallGO.GetComponent<SpriteRenderer>().sprite = wallHorizontal;
			 GameController.Instance.dottedLineGO.GetComponent<SpriteRenderer>().sprite = dottedLine;
			 GameController.Instance.leftRacketGO.GetComponent<SpriteRenderer>().sprite = racket;
			 GameController.Instance.rightRacketGO.GetComponent<SpriteRenderer>().sprite = racket;
			 GameController.Instance.ball.gameObject.GetComponent<SpriteRenderer>().sprite = ball;

			 // Unload the AssetBundles compressed contents to conserve memory
      		  bundle.Unload(false);
		}
     } // memory is freed from the web stream (www.Dispose() gets called implicitly)
   }
}