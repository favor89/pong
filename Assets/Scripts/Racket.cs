﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Racket : MonoBehaviour {
	public float speed = 30f;
	public string inputName = "RacketLeft";

	private PhotonView photonView;
	private Rigidbody2D rigid;

	void Awake(){
		this.rigid = GetComponent<Rigidbody2D> ();
		this.photonView = GetComponent<PhotonView> ();
	}
		
	void LateUpdate(){
		if (photonView.isMine) {
			float v = Input.GetAxisRaw (inputName);
			this.rigid.velocity = new Vector2 (0, v) * speed;
			// if there is any value on the input, notify GameController
			if (!Mathf.Approximately (v, 0))
				GameController.Instance.RacketDidMove (gameObject);
		}
	}
}
