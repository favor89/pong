﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
	public float speed = 30;

	private PhotonView photonView;
	private Rigidbody2D rigid;

	void Awake(){
		this.rigid = GetComponent<Rigidbody2D> ();
		this.photonView = GetComponent<PhotonView> ();
	}
		
	public void PrepareServe(bool leftHasTurn){
		if (photonView.isMine) {
			GameObject racketGO = leftHasTurn ? GameController.Instance.leftRacketGO : GameController.Instance.rightRacketGO;
			float offset = leftHasTurn ? RacketWidth () : -RacketWidth ();
			float ballXPosition = racketGO.transform.position.x + offset;
			// set the ball next to the player that has a turn
			transform.position = new Vector2 (ballXPosition, 0);
			// stop the ball
			rigid.velocity = Vector2.zero;
			photonView.RPC ("Sync", PhotonTargets.Others, transform.position, rigid.velocity);
		}
	}

	public void Serve(bool leftTurn){
		float xDir = leftTurn ? 1 : -1;
		Vector2 dir = new Vector2 (xDir, 0);
		// set the new velocity of the ball
		rigid.velocity = dir * speed;
		// sync position and velocity with the second client
		photonView.RPC ("Sync", PhotonTargets.Others, transform.position, rigid.velocity);
	}
		
	void OnCollisionEnter2D(Collision2D col) {
		// ignore collisions when in the serve state
		if (GameController.Instance.serve)
			return;
		
			// check if the ball touched one of the rackets
		if (col.gameObject == GameController.Instance.leftRacketGO || col.gameObject == GameController.Instance.rightRacketGO) {
			if (photonView.isMine) {
				// calculate y change based on point where ball touched the racket
				float y = HitFactor (transform.position, col.transform.position, col.collider.bounds.size.y);
				// change x direction to the opposite
				float xDir = col.contacts [0].point.x < 0 ? 1 : -1;
				Vector2 dir = new Vector2 (xDir, y).normalized;
				// set the new velocity of the ball
				rigid.velocity = dir * speed;
			} else {
				// if the opponent touched the ball, it means we'll need the ownership now
				photonView.RequestOwnership ();
			}
		} else if (col.gameObject == GameController.Instance.leftWallGO || col.gameObject == GameController.Instance.rightWallGO) {
			// ^ check if the ball touched one of the side walls (which means a goal)
			if (photonView.isMine) {
				GameController.Instance.BallDidTouchWall (col.gameObject == GameController.Instance.leftWallGO);
			}
		}
		if (photonView.isMine) {
			photonView.RPC ("Sync", PhotonTargets.Others, transform.position, rigid.velocity);
		}
	}


	[PunRPC]
	void Sync(Vector3 pos, Vector2 vel){
		// sync both position and velocity
		transform.position = pos;
		rigid.velocity = vel;
	}

	// helper methods
	private float HitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight) {
		return (ballPos.y - racketPos.y) / racketHeight;
	}
	private float RacketWidth(){
		return GameController.Instance.leftRacketGO.GetComponent<BoxCollider2D>().bounds.size.x;
	}
}
