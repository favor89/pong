﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	public ScoreTracker scoreTracker;
	public Ball ball;

	// references to game objects that have to be recognised while colliding with the ball
	public GameObject leftRacketGO;
	public GameObject rightRacketGO;
	public GameObject ballGO;
	public GameObject dottedLineGO;
	public GameObject bottomWallGO;
	public GameObject topWallGO;
	public GameObject leftWallGO;
	public GameObject rightWallGO;


	public bool serve; // indicades the state of the game
	private bool leftHasTurn; // indicates who will serve

	// initial positions are stored to reset rackets position after a goal
	private Vector2 leftRacketInitPos;
	private Vector2 rightRacketInitPos;

	private PhotonView photonView;

	public static GameController Instance
	{
		get;
		private set;
	}

	void Awake(){
		Instance = this;
		this.photonView = GetComponent<PhotonView> ();
	}
	void Start(){
		// assign initial positions
		leftRacketInitPos = leftRacketGO.transform.position;
		rightRacketInitPos = rightRacketGO.transform.position;

		// left will start
		leftHasTurn = true;
		// set the serve state
		serve = true;
		// prepare the ball
		ball.PrepareServe (leftHasTurn);
	}
		
	public void OnJoinedRoom(){
		// assign master to the left racket and slave to the right
		if (PhotonNetwork.isMasterClient) {
			leftRacketGO.GetComponent<PhotonView> ().RequestOwnership ();
		} else {
			rightRacketGO.GetComponent<PhotonView> ().RequestOwnership ();
		}
	}
		
	public void RacketDidMove(GameObject racketGO){
		// handle it only if in serve state
		if (serve) {
			bool left = racketGO == leftRacketGO;
			if (left && leftHasTurn || !left && !leftHasTurn) {
				// if the serving player did move, serve the ball and change the state to non-serve
				ball.Serve (leftHasTurn);
				serve = false;
			}
		}
	}
	public void BallDidTouchWall(bool left){
		Goal (left);
		// notify the other player
		photonView.RPC ("Goal", PhotonTargets.Others, left);
	}

	[PunRPC]
	void Goal(bool left){
		// somebody scored a goal! update the score tracker
		scoreTracker.Scored (!left);
		// pass the turn to the player that lost a goal
		leftHasTurn = left;
		// set the serve state
		serve = true;
		// prepare the ball
		ball.PrepareServe (leftHasTurn);
		// prepare the rackets
		leftRacketGO.transform.position = leftRacketInitPos;
		rightRacketGO.transform.position = rightRacketInitPos;
	}
}
