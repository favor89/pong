﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTracker : MonoBehaviour {
	public Text leftScoreText;
	public Text rightScoreText;

	private int leftScore = 0;
	private int rightScore = 0;

	public void Scored(bool left){
		if (left) {
			leftScore++;
		} else {
			rightScore++;
		}
		UpdateTexts ();
	}
	private void UpdateTexts(){
		leftScoreText.text = leftScore.ToString();
		rightScoreText.text = rightScore.ToString();
	}
}
