# Pong
A simple Pong game.

## How to run
- Create two instances of the game (one can be played in Unity3D editor and the second can be a standalone build)
- Run both of them
- Wait until network status GUI disappear on both clients
- Play the left racket W/S and the right Up/Down

## Todo
- Let the players play when and only when there are two of them
- Handle leaving/rejoining players
- Fix bugs (didn't appear while playing locally, but did in an online game)
- Create a mechanism for changing assets via Asset Bundles
- Improve dynamic assets mechanism
	- Add caching
	- Add checking version on the server-side
	- Add checking bundle name on the server-side
	- Load assets asynchronously

## History
- v4 (April 6 2017, 4:11PM): Updating assets via AssetBundle
	- Simple version without caching, firing on the start of the app
	- All assets loaded synchronously 

- v3 (April 5 2017, 3:20PM): Online version of gameplay
	- Photon Unity Networking plugin added
	- Setting the proper ownership for both left and right racket
	- Changing the ownership of the ball after hitting one of the rackets
	- Syncing the position and velocity of the ball after each collision (discrete events)
	- Syncing the positions of the rackets by interpolating their positions (continuously)

- v2 (April 5 2017, 1:30PM): A bit more complex gameplay
	- A new (left) racket object added
	- Two inputs defined in Input settings (the left player plays with W/S, the right player with Up/Down buttons)
	- Two bool states introduced: serve and leftHasTurn
	- Preparing the serve state by moving the rackets in their initial positions and the ball next to the one that serves
	
- v1 (April 5 2017, 12:31AM): A simple gameplay: single player playing with the wall
	- BoxCollider2D component on the wall objects, the ball object and the racket object
	- Rigidbody2D component on the ball objects and the racket object
	- PhysicsMaterial2D component on the ball
	- GameController as a Singleton
	- Simple UI for displaying the score

## Author
Wojciech Roszkowiak
